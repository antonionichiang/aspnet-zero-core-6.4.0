﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace Abp.AspNetZeroCore.V
{
   internal class PrivateImplementationDetails
    {
        internal static readonly StaticArrayInitTypeSize1 staticArrayInitTypeSize1=new StaticArrayInitTypeSize1();

        internal static readonly StaticArrayInitTypeSize2 staticArrayInitTypeSize2 = new StaticArrayInitTypeSize2();

        [StructLayout(LayoutKind.Explicit, Pack = 1, Size = 80)]
        internal struct StaticArrayInitTypeSize1
        {
        }

        // Token: 0x02000017 RID: 23
        [StructLayout(LayoutKind.Explicit, Pack = 1, Size = 116)]
        internal struct StaticArrayInitTypeSize2
        {
        }
    }
}
