﻿using System;
using System.Runtime.CompilerServices;

namespace Abp.AspNetZeroCore.Licensing
{
	public class DecryptedCheckCodeInfo
    {
        public bool Succeed { get; set; } = true;

		public DateTime CheckTime { get;  set; }=DateTime.Now;

		public DecryptedCheckCodeInfo()
		{
			Console.WriteLine("DecryptedCheckCodeInfo");
		}
	}
}
