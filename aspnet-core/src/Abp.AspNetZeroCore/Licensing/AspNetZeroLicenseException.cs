﻿using System;
using System.Runtime.CompilerServices;

namespace Abp.AspNetZeroCore.Licensing
{
	public class AspNetZeroLicenseException : Exception
	{
		public AspNetZeroLicenseException():this("AspNet Zero License Check Failed. Please contact to info@aspnetzero.com if you are using a licensed version!")
		{
            Console.WriteLine("AspNetZeroLicenseException");
		}

		public AspNetZeroLicenseException(string message):base(message)
		{

		}
	}
}
