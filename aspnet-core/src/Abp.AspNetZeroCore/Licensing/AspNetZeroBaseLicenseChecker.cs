﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Abp.Authorization.Users;
using Abp.Zero.Configuration;

namespace Abp.AspNetZeroCore.Licensing
{
	public abstract class AspNetZeroBaseLicenseChecker
	{
		private string LicenseCode { get; }

		protected abstract string GetSalt();

		protected abstract string GetHashedValueWithoutUniqueComputerId(string str);

        private readonly IAbpZeroConfig _abpZeroConfig;

        protected AspNetZeroBaseLicenseChecker(AspNetZeroConfiguration configuration, IAbpZeroConfig abpZeroConfig, string configFilePath = "")
        {
               _abpZeroConfig = abpZeroConfig;
        }

		protected string GetLicenseCode()
		{
			return this.LicenseCode;
		}

		protected bool CompareProjectName(string hashedProjectName)
		{
			return true;
		}

		// Token: 0x06000028 RID: 40 RVA: 0x000024B4 File Offset: 0x000008B4
		[MethodImpl(MethodImplOptions.NoInlining)]
		protected string GetAssemblyName()
		{
			return "";
		}

		protected string GetLicenseController()
		{
			return "WebProject";
		}

		protected bool IsThereAReasonToNotCheck()
		{
			return !Debugger.IsAttached;
		}

		
	}
}
