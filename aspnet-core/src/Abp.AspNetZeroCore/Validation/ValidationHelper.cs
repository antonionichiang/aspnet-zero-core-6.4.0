﻿using System;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using Abp.Extensions;

namespace Abp.AspNetZeroCore.Validation
{
	public static class ValidationHelper
	{
		public static bool IsEmail(string value)
		{
			return !ValidationHelper.IsNullOrEmpty(value) && ValidationHelper.IsMatch(new Regex(EmailRegex), value);
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		internal static bool IsNullOrEmpty(string text)
		{
			return text.IsNullOrEmpty();
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		internal static bool IsMatch(Regex regex, string input)
		{
			return regex.IsMatch(input);
		}

		public const string EmailRegex = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
	}
}
