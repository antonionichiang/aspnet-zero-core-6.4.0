﻿using System;
using System.Text.RegularExpressions;

namespace Abp.AspNetZeroCore.Web.Url
{

	public static class UrlChecker
	{

		public static bool IsRooted(string url)
		{
			return url.StartsWith("/") || UrlChecker.UrlWithProtocolRegex.IsMatch(url);
		}

		private static readonly Regex UrlWithProtocolRegex = new Regex("^.{1,10}://.*$");
	}
}
