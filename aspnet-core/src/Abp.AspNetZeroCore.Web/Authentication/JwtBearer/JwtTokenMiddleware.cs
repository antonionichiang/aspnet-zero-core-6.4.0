﻿using System;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace Abp.AspNetZeroCore.Web.Authentication.JwtBearer
{

	public static class JwtTokenMiddleware
	{

		public static IApplicationBuilder UseJwtTokenMiddleware(this IApplicationBuilder app, string schema = "Bearer")
        {
            return app.Use(async (ctx, next) =>
            {
                IIdentity identity = ctx.User.Identity;
                if (identity == null || !identity.IsAuthenticated)
                {
                    var val = await ctx.AuthenticateAsync(schema);
                    if (val.Succeeded && val.Principal != null)
                    {
                        ctx.User = val.Principal;
                    }
                }

                await next();
            });
        }
	}
}
