﻿using System;

namespace Abp.AspNetZeroCore.Web.Authentication.External
{
	// Token: 0x02000008 RID: 8
	public class ExternalAuthUserInfo
	{
		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000013 RID: 19 RVA: 0x0000226D File Offset: 0x0000046D
		// (set) Token: 0x06000014 RID: 20 RVA: 0x00002275 File Offset: 0x00000475
		public string ProviderKey { get; set; }

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000015 RID: 21 RVA: 0x0000227E File Offset: 0x0000047E
		// (set) Token: 0x06000016 RID: 22 RVA: 0x00002286 File Offset: 0x00000486
		public string Name { get; set; }

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000017 RID: 23 RVA: 0x0000228F File Offset: 0x0000048F
		// (set) Token: 0x06000018 RID: 24 RVA: 0x00002297 File Offset: 0x00000497
		public string EmailAddress { get; set; }

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000019 RID: 25 RVA: 0x000022A0 File Offset: 0x000004A0
		// (set) Token: 0x0600001A RID: 26 RVA: 0x000022A8 File Offset: 0x000004A8
		public string Surname { get; set; }

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600001B RID: 27 RVA: 0x000022B1 File Offset: 0x000004B1
		// (set) Token: 0x0600001C RID: 28 RVA: 0x000022B9 File Offset: 0x000004B9
		public string Provider { get; set; }
	}
}
