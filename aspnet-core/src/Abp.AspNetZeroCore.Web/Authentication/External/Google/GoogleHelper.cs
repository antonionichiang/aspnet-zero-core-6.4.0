﻿using System;
using Newtonsoft.Json.Linq;

namespace Abp.AspNetZeroCore.Web.Authentication.External.Google
{
	// Token: 0x02000011 RID: 17
	public static class GoogleHelper
	{
		// Token: 0x0600003C RID: 60 RVA: 0x0000253D File Offset: 0x0000073D
		public static string GetId(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("id");
		}

		// Token: 0x0600003D RID: 61 RVA: 0x00002558 File Offset: 0x00000758
		public static string GetName(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("displayName");
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00002573 File Offset: 0x00000773
		public static string GetGivenName(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return GoogleHelper.TryGetValue(user, "name", "givenName");
		}

		// Token: 0x0600003F RID: 63 RVA: 0x00002593 File Offset: 0x00000793
		public static string GetFamilyName(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return GoogleHelper.TryGetValue(user, "name", "familyName");
		}

		// Token: 0x06000040 RID: 64 RVA: 0x000025B3 File Offset: 0x000007B3
		public static string GetProfile(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("url");
		}

		// Token: 0x06000041 RID: 65 RVA: 0x000025CE File Offset: 0x000007CE
		public static string GetEmail(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return GoogleHelper.TryGetFirstValue(user, "emails", "value");
		}

		// Token: 0x06000042 RID: 66 RVA: 0x000025F0 File Offset: 0x000007F0
		private static string TryGetValue(JObject user, string propertyName, string subProperty)
		{
			if (user.TryGetValue(propertyName, out var jtoken))
			{
				JObject jobject = JObject.Parse(jtoken.ToString());
				if (jobject != null && jobject.TryGetValue(subProperty, out jtoken))
				{
					return jtoken.ToString();
				}
			}
			return null;
		}

		// Token: 0x06000043 RID: 67 RVA: 0x0000262C File Offset: 0x0000082C
		private static string TryGetFirstValue(JObject user, string propertyName, string subProperty)
		{
		
			if (user.TryGetValue(propertyName, out var jtoken))
			{
				JArray jarray = JArray.Parse(jtoken.ToString());
				if (jarray != null && jarray.Count > 0)
				{
					JObject jobject = JObject.Parse(jarray.First.ToString());
					if (jobject != null && jobject.TryGetValue(subProperty, out jtoken))
					{
						return jtoken.ToString();
					}
				}
			}
			return null;
		}
	}
}
