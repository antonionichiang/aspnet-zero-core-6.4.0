﻿using System;
using System.Threading.Tasks;

namespace Abp.AspNetZeroCore.Web.Authentication.External
{
	// Token: 0x0200000C RID: 12
	public interface IExternalAuthProviderApi
	{
		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600002C RID: 44
		ExternalLoginProviderInfo ProviderInfo { get; }

		// Token: 0x0600002D RID: 45
		Task<bool> IsValidUser(string userId, string accessCode);

		// Token: 0x0600002E RID: 46
		Task<ExternalAuthUserInfo> GetUserInfo(string accessCode);

		// Token: 0x0600002F RID: 47
		void Initialize(ExternalLoginProviderInfo providerInfo);
	}
}
