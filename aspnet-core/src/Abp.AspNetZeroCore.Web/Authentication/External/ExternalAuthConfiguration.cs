﻿using System;
using System.Collections.Generic;
using Abp.Dependency;

namespace Abp.AspNetZeroCore.Web.Authentication.External
{
	// Token: 0x02000005 RID: 5
	public class ExternalAuthConfiguration : IExternalAuthConfiguration, ISingletonDependency
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000007 RID: 7 RVA: 0x000020D4 File Offset: 0x000002D4
		public List<ExternalLoginProviderInfo> Providers { get; }

		// Token: 0x06000008 RID: 8 RVA: 0x000020DC File Offset: 0x000002DC
		public ExternalAuthConfiguration()
		{
			this.Providers = new List<ExternalLoginProviderInfo>();
		}
	}
}
