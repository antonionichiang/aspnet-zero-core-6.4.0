﻿using System;
using Newtonsoft.Json.Linq;

namespace Abp.AspNetZeroCore.Web.Authentication.External.Microsoft
{
	// Token: 0x0200000E RID: 14
	public static class MicrosoftAccountHelper
	{
		// Token: 0x06000033 RID: 51 RVA: 0x0000240A File Offset: 0x0000060A
		public static string GetId(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("id");
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002425 File Offset: 0x00000625
		public static string GetDisplayName(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("displayName");
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002440 File Offset: 0x00000640
		public static string GetGivenName(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("givenName");
		}

		// Token: 0x06000036 RID: 54 RVA: 0x0000245B File Offset: 0x0000065B
		public static string GetSurname(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("surname");
		}

		// Token: 0x06000037 RID: 55 RVA: 0x00002476 File Offset: 0x00000676
		public static string GetEmail(JObject user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.Value<string>("mail") ?? user.Value<string>("userPrincipalName");
		}
	}
}
