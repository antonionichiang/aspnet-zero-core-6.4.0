﻿using System;
using System.Threading.Tasks;
using Abp.Dependency;

namespace Abp.AspNetZeroCore.Web.Authentication.External
{
	// Token: 0x02000007 RID: 7
	public abstract class ExternalAuthProviderApiBase : IExternalAuthProviderApi, ITransientDependency
	{
		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600000D RID: 13 RVA: 0x000021F5 File Offset: 0x000003F5
		// (set) Token: 0x0600000E RID: 14 RVA: 0x000021FD File Offset: 0x000003FD
		public ExternalLoginProviderInfo ProviderInfo { get; set; }

		// Token: 0x0600000F RID: 15 RVA: 0x00002206 File Offset: 0x00000406
		public void Initialize(ExternalLoginProviderInfo providerInfo)
		{
			this.ProviderInfo = providerInfo;
		}

		// Token: 0x06000010 RID: 16 RVA: 0x00002210 File Offset: 0x00000410
		public async Task<bool> IsValidUser(string userId, string accessCode)
		{
			return (await this.GetUserInfo(accessCode)).ProviderKey == userId;
		}

		// Token: 0x06000011 RID: 17
		public abstract Task<ExternalAuthUserInfo> GetUserInfo(string accessCode);
	}
}
