﻿using System;
using System.Collections.Generic;

namespace Abp.AspNetZeroCore.Web.Authentication.External
{
	// Token: 0x0200000A RID: 10
	public interface IExternalAuthConfiguration
	{
		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000029 RID: 41
		List<ExternalLoginProviderInfo> Providers { get; }
	}
}
